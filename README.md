# Welcome to Better Netdata Notifs
A better notification engine for netdata.

# Why?
I've always had netdata notifications on for my servers, but 99% of the time the notifications I get are useless and don't matter to me.

So, I decided why not make a script to get important notifications on Slack about machines from Netdata, and allow easy customization of a variety of alerts.

# Timeline
The timeline of this project is as follows:
* until mid-2018: Experimental phase (testing Slack & Netdata API)
* until early 2019: Development phase - Add in all the usual triggers and notifications
* maybe late 2019: Main development done, MAYBE add in custom triggers
